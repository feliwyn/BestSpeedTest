#!/usr/bin/python2

import subprocess
import argparse

statistics=[]

# MAKE ALL SPEED TEST 
# STORE THEM IN STATISTICS[]
def MakeSpeedTest():
    speedtest_result = subprocess.check_output(['speedtest-cli', '--share'])
    
    download = speedtest_result.split("Download: ")[1].split(" Mbit")[0]
    upload = speedtest_result.split("Upload: ")[1].split(" Mbit")[0]
    moyenne = (float(download)+float(upload))/2
    share_links = speedtest_result.split("Share results: ")[1].split("\n")[0]

    return [download,upload,share_links,moyenne]

#SORT ALL SPEEDTEST IN STATISTICS
#AND FIND THE BEST ONE (moyenne/upload/download)
def CheckTheBestOne(sort=0):
    best=0
    tmpBest=0
    
    
    for number in range(0,int(args.number)):
        if tmpBest < statistics[number][sort]:
            best=number
            tmpBest=statistics[number][sort]
    return best

def AllResult(number,sort=None):
    #print all result
    sorted_stat=sorted(statistics, key=lambda stat: stat[sort])
    print("")
    print("")
    print("TOP\tDOWNLOAD\tUPLOAD\t\tMOYENNE\t\tLIEN")
    if sort == 0:
        for i in range(0,number):
            print(str(number-i)+"\t|"+str(sorted_stat[i][0])+"|\t"+str(sorted_stat[i][1])+"\t\t"+str(sorted_stat[i][3])+"\t\t"+str(sorted_stat[i][2]))
    elif sort == 1:
        for i in range(0,number):
            print(str(number-i)+"\t"+str(sorted_stat[i][0])+"\t\t|"+str(sorted_stat[i][1])+"|\t"+str(sorted_stat[i][3])+"\t\t"+str(sorted_stat[i][2]))
    elif sort == 3:
        for i in range(0,number):
            print(str(number-i)+"\t"+str(sorted_stat[i][0])+"\t\t"+str(sorted_stat[i][1])+"\t\t|"+str(sorted_stat[i][3])+"|\t"+str(sorted_stat[i][2]))
        
        
        
        

# PARSER ARGUMENTS HERE
parser = argparse.ArgumentParser(description="Make multiple speedtest, and take the best one")
parser.add_argument('-s', '--sort', action='store', dest='sort', help="Sort by 0=moyenne, 1=download, 2=upload", default=0)
parser.add_argument('-a', '--all', action='store_true', default=False, help="If used, show all result of the speedtest.")
parser.add_argument('number', help="Number of speedtest")
args=parser.parse_args()



# START CODE HERE
if __name__ == "__main__":
    
    # sort referent to the column in statistics[]
    # column 0 : download
    # column 1 : upload
    # column 2 : lien
    # column 3 : moyenne
    
    
    if int(args.sort) == 0:
        sort=3
    elif int(args.sort) == 1:
        sort=0
    elif int(args.sort) == 2:
        sort=1
    else:
        print("[-] Wrong sort arguments. Only digit")
        print("0=moyenne, 1=download, 2=upload")
        quit()
        
        
    numberOfSpeedtest=int(args.number)
    print(str(numberOfSpeedtest)+" test(s) to do")
    
    for number in range(0, numberOfSpeedtest):
        print("  #"+str(number+1)+" ...")
        statistics.extend([MakeSpeedTest()])
    
    print("")
    print("[+] ALL SPEEDTEST FINISHED")
        
    b=CheckTheBestOne(sort)
    
    if args.all == True:
        AllResult(numberOfSpeedtest, sort)
    
    print("Download: "+str(statistics[b][0]))
    print("Upload: "+str(statistics[b][1]))
    print("Share: "+str(statistics[b][2]))
    


